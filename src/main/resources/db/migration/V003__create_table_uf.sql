create table uf(
	id bigint not null auto_increment,
	nome varchar(80) not null,
	primary key(id)	
);

INSERT INTO uf(nome) SELECT DISTINCT nome_estado FROM cidade;

ALTER TABLE cidade
ADD COLUMN uf_id bigint not null;

UPDATE cidade c SET c.uf_id = (
	SELECT u.id FROM uf u WHERE u.nome = c.nome_estado
); 

ALTER TABLE cidade ADD CONSTRAINT fk_uf_id FOREIGN KEY(uf_id) REFERENCES uf(id); 

ALTER TABLE cidade CHANGE nome_cidade nome VARCHAR(80) NOT NULL;

ALTER TABLE cidade DROP COLUMN nome_estado;