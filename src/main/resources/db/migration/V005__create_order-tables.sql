CREATE TABLE pedido(
	id bigint not null auto_increment,
	sub_Total decimal(10,2) not null,
	taxa_frete decimal(10,2) not null,
	valor_total decimal(10,2) not null,
	data_criacao datetime not null,
	data_confirmacao datetime,
	dataCancelamento datetime,
	dataEntrega datetime,
	status int not null,
	endereco_cep varchar(9) not null,
	endereco_logradouro varchar(100) not null,
	endereco_numero varchar(40),
	endereco_complemento varchar(100),
	endereco_bairro varchar(100),
	forma_pagamento_id bigint not null,
	usuario_id bigint not null,
	restaurante_id bigint not null,	
	primary key(id)
); 

ALTER TABLE pedido ADD CONSTRAINT fk_pedido_forma_pagamento FOREIGN KEY pedido(forma_pagamento_id) REFERENCES forma_pagamento(id);
ALTER TABLE pedido ADD CONSTRAINT fk_pedido_usuario FOREIGN KEY pedido(usuario_id) REFERENCES usuario(id);
ALTER TABLE pedido ADD CONSTRAINT fk_pedido_restaurante FOREIGN KEY pedido(restaurante_id) REFERENCES restaurante(id);

CREATE TABLE item_pedido(
	id bigint not null auto_increment,
	quantidade bigint not null,
	preco_unitario decimal(10,2) not null,
	preco_total decimal(10,2) not null,
	observacao VARCHAR(130),
	pedido_id bigint not null,
	primary key(id)
);

ALTER TABLE item_pedido ADD CONSTRAINT fk_item_pedido_pedido FOREIGN KEY item_pedido(pedido_id) REFERENCES pedido(id); 

