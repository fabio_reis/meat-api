package com.algaworks.meat.infraestructure;

import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@SuppressWarnings("unchecked")
public abstract class BaseController<M,DTO,INPUT> {	
	
	@Autowired
	private ModelMapper mapper;	
	
	private Class<M> model = (Class<M>)((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	private Class<DTO> dto = (Class<DTO>)((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[1];
	private Class<INPUT> input = (Class<INPUT>)((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[2];
	
	protected DTO toModelDTO(M m) {		
		return mapper.map(m, dto);	
	}
	
	protected INPUT toModelInput(M model) {
		return mapper.map(model, input);
	}
	
	protected M toModel(INPUT input) {		
		return (M) mapper.map(input, model);
	}
	
	protected List<DTO> toCollectionModelDTO(List<M> models){
		return models.stream()
				.map(model -> toModelDTO(model))
				.collect(Collectors.toList());
	}		
}
