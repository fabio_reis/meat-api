	package com.algaworks.meat.infraestructure;

import java.time.OffsetDateTime;
import java.util.stream.Collectors;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.algaworks.meat.domain.exception.BussinessException;
import com.algaworks.meat.domain.exception.EmailRegisteredException;
import com.algaworks.meat.domain.exception.EntityDependenceNotFoundException;
import com.algaworks.meat.domain.exception.EntityInUseException;
import com.algaworks.meat.domain.exception.EntityNotFoundException;
import com.algaworks.meat.domain.exception.EntityRegistredException;
import com.algaworks.meat.domain.exception.PasswordException;
import com.algaworks.meat.domain.exception.ValueRequiredException;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.fasterxml.jackson.databind.exc.PropertyBindingException;

@SuppressWarnings({"unused","rawtypes"})
@RestControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {	
	
	@Autowired
	private MessageSource messageSource;	
	
	private ErrorDetail errorDetail = new ErrorDetail();
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity handleUncaught(Exception e, WebRequest request) {		
		
		errorDetail.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		errorDetail.setDetail(messageSource.getMessage("meat.user.message", null,null));		
		
		e.printStackTrace();
		
		return handleExceptionInternal(e, errorDetail, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
	}
	
	@ExceptionHandler({EntityDependenceNotFoundException.class, 
		ValueRequiredException.class, EmailRegisteredException.class, PasswordException.class})
	public ResponseEntity handleBadRequest(BussinessException e, 
			WebRequest request) {		
		
		return handleExceptionInternal(e, e.getMessage(), new HttpHeaders(),
				HttpStatus.BAD_REQUEST, request); 
	}	
	
	@ExceptionHandler({EntityInUseException.class, 
		EntityRegistredException.class})
	public ResponseEntity handleConflict(BussinessException e, 
			WebRequest request) {		
		
		return handleExceptionInternal(e, e.getMessage(), new HttpHeaders(),
				HttpStatus.CONFLICT, request); 		
	}
	
	@ExceptionHandler(EntityNotFoundException.class)
	public ResponseEntity handleNotFound(EntityNotFoundException e, 
			WebRequest request) {				
		
		return handleExceptionInternal(e, e.getMessage(), new HttpHeaders(),
				HttpStatus.NOT_FOUND, request);		
	}
	
	@Override
	protected ResponseEntity<Object> handleTypeMismatch(
			TypeMismatchException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {			
		
		if(ex instanceof MethodArgumentTypeMismatchException) 
			return handleMethodArgumentTypeMismatch((MethodArgumentTypeMismatchException)ex, headers, status, request);			
		
		return super.handleTypeMismatch(ex, headers, status, request);
		
	}	
	
	private ResponseEntity<Object> handleMethodArgumentTypeMismatch(MethodArgumentTypeMismatchException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {	
		
		errorDetail.setStatus(status.value());
		errorDetail.setDetail(String.format("O parâmetro de URL '%s' recebeu o valor '%s', "
            + "que é de um tipo inválido. Corrija e informe um valor compatível com o tipo %s.", ex.getName(),
				ex.getValue(), ex.getRequiredType().getSimpleName()));
		
		return super.handleExceptionInternal(ex, errorDetail, headers, status, request);
		
	}
	
	
	@Override
	protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		
		Throwable rootCause = ExceptionUtils.getRootCause(ex);
		
		if(rootCause instanceof InvalidFormatException) 
			return handleInvalidFormatException((InvalidFormatException)rootCause, headers, status, request);
		
		if(rootCause instanceof PropertyBindingException) 
			return handlePropertyBindingException((PropertyBindingException)rootCause, headers, status, request);			
		
		errorDetail.setStatus(status.value());
		errorDetail.setDetail("Corpo da requisição está inválido.");		
		
		return super.handleExceptionInternal(ex, errorDetail, headers, status, request);
	}
	
	
	private ResponseEntity<Object> handleInvalidFormatException(InvalidFormatException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		
		String path = ex.getPath().stream()
				.map(ref -> ref.getFieldName()).collect(Collectors.joining("."));			
		
		errorDetail.setStatus(status.value());
		errorDetail.setDetail(String.format("A propriedade '%s' deve ser do tipo '%s'",
				path,ex.getTargetType().getSimpleName()));		
		
		return super.handleExceptionInternal(ex, errorDetail, headers, status, request);
	}
	
	private ResponseEntity<Object> handlePropertyBindingException(PropertyBindingException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		
		String path = ex.getPath().stream()
				.map(ref -> ref.getFieldName()).collect(Collectors.joining("."));			
		
		errorDetail.setStatus(status.value());
		errorDetail.setDetail(String.format("A propriedade '%s' não existe",path));		
		
		return super.handleExceptionInternal(ex, errorDetail, headers, status, request);
	}

	@Override
	protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers,
			HttpStatus status, WebRequest request) {	
		
		errorDetail.setStatus(status.value());		
		errorDetail.setDate(OffsetDateTime.now());
		errorDetail.setUserMessage(messageSource.getMessage("meat.user.message",null,null));
		
		if(body == null) {			
			errorDetail.setDetail(status.getReasonPhrase());
			body = errorDetail;
		}else if(body instanceof String) {			
			errorDetail.setDetail((String)body);
			body = errorDetail;
		}		
		
		return super.handleExceptionInternal(ex, body, headers, status, request);
	}	
	
	@JsonInclude(Include.NON_NULL)
	private class ErrorDetail{		
		
		@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.XXX")
		private OffsetDateTime date;				
		private String detail;
		private Integer status;
		private String userMessage;		
		
		public String getDetail() {return detail;}		
		public void setDetail(String detail) {this.detail = detail;}

		public Integer getStatus() {return status;}
		public void setStatus(Integer status) {this.status = status;}
		
		public String getUserMessage() {return userMessage;}		
		public void setUserMessage(String userMessage) {this.userMessage = userMessage;}
		
		public OffsetDateTime getDate() {return date;}
		public void setDate(OffsetDateTime date) {this.date = date;}				
				
	}

}
