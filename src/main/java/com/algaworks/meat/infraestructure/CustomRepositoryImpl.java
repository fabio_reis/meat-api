package com.algaworks.meat.infraestructure;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.util.ObjectUtils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class CustomRepositoryImpl<T, ID> extends SimpleJpaRepository<T, ID>
	implements CustomRepository<T, ID>{	
	
	private final String AND = " AND ";
	private final String EQ = "=";
	
	public CustomRepositoryImpl(JpaEntityInformation<T, ?> entityInformation, EntityManager entityManager) {
		super(entityInformation, entityManager);
		this.manager = entityManager;
	}	
	
	private EntityManager manager;		
	
	public List<T> filter(T filter){
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> parameters = mapper.convertValue(filter, new TypeReference<Map<String, Object>>() {});	
		StringBuilder jpql = new StringBuilder("from " + getDomainClass().getName() + " WHERE 1=1 ");			
		
		appendConditionalJpql(parameters,jpql);			
		
		TypedQuery<T> query = manager.createQuery(jpql.toString(), getDomainClass());			
		
		appendQueryConditional(parameters,query);
		
		return query.getResultList();				
	}
	
	private void appendConditionalJpql(Map<String, Object> parameters, StringBuilder jpql) {		
		parameters.forEach((key,value) -> {
			if(!ObjectUtils.isEmpty(value)) 
				jpql.append(AND + key + EQ + ":"+key);						
		});		
	}
	
	private void appendQueryConditional(Map<String, Object> parameters, Query query) {			
		parameters.forEach((key,value) -> {
			if(!ObjectUtils.isEmpty(value)) 
				query.setParameter(key, value);						
		});	
	}
}
