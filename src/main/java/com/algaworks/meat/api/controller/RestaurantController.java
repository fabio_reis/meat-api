package com.algaworks.meat.api.controller;

import java.lang.reflect.Field;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.algaworks.meat.api.model.dto.RestaurantDTO;
import com.algaworks.meat.api.model.input.AddressInput;
import com.algaworks.meat.api.model.input.RestaurantInput;
import com.algaworks.meat.domain.model.Restaurant;
import com.algaworks.meat.domain.service.RestaurantService;
import com.algaworks.meat.infraestructure.BaseController;
import com.fasterxml.jackson.databind.ObjectMapper;


@SuppressWarnings("rawtypes")
@RestController
@RequestMapping(path = "/restaurants", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
public class RestaurantController extends BaseController<Restaurant, RestaurantDTO, RestaurantInput> {
	
	@Autowired
	private RestaurantService service;	
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@GetMapping()
	public ResponseEntity list(Restaurant filter){			
		return ResponseEntity.ok()
				.body(toCollectionModelDTO(this.service.list()));
	}	
	
	@GetMapping("/{id}")
	public ResponseEntity findById(@PathVariable Long id) {								
		return ResponseEntity.ok()
				.body(toModelDTO(this.service.findById(id)));
			
	}
	
	@PostMapping	
	@ResponseStatus(HttpStatus.CREATED)
	public void save(@RequestBody RestaurantInput restaurantInput) {		
		this.service.persist(toModel(restaurantInput));				
	}
	
	@PatchMapping("/{id}/address")	
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void patchAddress(@PathVariable Long id, @RequestBody AddressInput addressInput) {			
		RestaurantInput restaurantInput = toModelInput(this.service.findById(id));
		restaurantInput.setAddress(addressInput);		 
		this.service.persist(toModel(restaurantInput));				
	}
	
	@PutMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public void update(@PathVariable Long id, @RequestBody RestaurantInput restaurantInput) {				
		Restaurant restaurant = this.service.findById(id);	
		BeanUtils.copyProperties(restaurantInput,restaurant,"id","paymentMethods",
				"address", "dtCreation");					
		this.service.persist(restaurant);					
	}
	
	@PutMapping("/{id}/activate")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void activate(@PathVariable Long id) {				
		this.service.activate(id);		
	}
	
	@PutMapping("/{id}/inactivate")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void inactivate(@PathVariable Long id) {						
		this.service.inactivate(id);		
	}
	
	@PatchMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public void updatePath(@PathVariable Long id, @RequestBody Map<String, Object> values, HttpServletRequest request) {			
		RestaurantInput restaurant = toModelInput(this.service.findById(id));			
		mergeFieldValues(values, restaurant, request);			
		restaurant.setId(id);			
		this.service.persist(toModel(restaurant));		
	}	
	
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Long id) {			
		this.service.delete(id);				
	}	
	
	private void mergeFieldValues(Map<String, Object> values, RestaurantInput restaurantInput,
			HttpServletRequest request) {		
		ServletServerHttpRequest ss = new ServletServerHttpRequest(request);
		try {			
			RestaurantInput restaurantValue = objectMapper.convertValue(values, RestaurantInput.class);
			
			values.forEach((propertyName, propertyValue) -> {
				Field field = ReflectionUtils.findField(RestaurantInput.class, propertyName);			
				field.setAccessible(true);
				
				Object newValue = ReflectionUtils.getField(field, restaurantValue);
				
				ReflectionUtils.setField(field, restaurantInput, newValue);				
			});		
		}catch(IllegalArgumentException e) {
			Throwable rootCause = ExceptionUtils.getRootCause(e);
			throw new HttpMessageNotReadableException(e.getMessage(), rootCause, ss);
		}		
		
	}

}
