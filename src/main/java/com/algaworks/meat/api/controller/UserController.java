package com.algaworks.meat.api.controller;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.algaworks.meat.api.model.dto.UserDTO;
import com.algaworks.meat.api.model.input.UserInput;
import com.algaworks.meat.api.model.input.UserInputWithoutPassword;
import com.algaworks.meat.api.model.input.UserPasswordInput;
import com.algaworks.meat.domain.model.User;
import com.algaworks.meat.domain.service.UserService;
import com.algaworks.meat.infraestructure.BaseController;

@SuppressWarnings("rawtypes")
@RestController
@RequestMapping(path = "/users", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
public class UserController extends BaseController<User, UserDTO, UserInput> {
	
	@Autowired
	private UserService service;		
	
	@GetMapping()	
	public ResponseEntity list(){				
		return ResponseEntity.ok()
				.body(toCollectionModelDTO(this.service.list()));		
	}
	
	@GetMapping("/{id}")
	public ResponseEntity findById(@PathVariable Long id) {							
		return ResponseEntity.ok()
				.body(toModelDTO(this.service.findById(id)));				
	}	
	
	@PostMapping	
	@ResponseStatus(HttpStatus.CREATED)
	public void save(@RequestBody UserInput userInput) {		
		this.service.persist(toModel(userInput));						
	}
	
	@PutMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public void update(@PathVariable Long id, @RequestBody UserInputWithoutPassword userInputWithoutpassword) {			
		User user = this.service.findById(id);			
		BeanUtils.copyProperties(userInputWithoutpassword, user,"id");		
		this.service.persist(user);			
	}
	
	@PutMapping("/{id}/password")
	@ResponseStatus(HttpStatus.OK)
	public void updatePassword(@PathVariable Long id, @RequestBody UserPasswordInput userPasswordInput) {			
		User user = this.service.findById(id);			
		BeanUtils.copyProperties(userPasswordInput, user,"id");		
		this.service.updatePassword(user);			
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Long id) {			
			this.service.delete(id);			
	}

}
