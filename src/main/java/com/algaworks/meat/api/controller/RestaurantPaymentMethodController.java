package com.algaworks.meat.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.algaworks.meat.api.model.dto.PaymentMethodDTO;
import com.algaworks.meat.api.model.input.PaymentMethodInput;
import com.algaworks.meat.domain.model.PaymentMethod;
import com.algaworks.meat.domain.model.Restaurant;
import com.algaworks.meat.domain.service.RestaurantService;
import com.algaworks.meat.infraestructure.BaseController;


@SuppressWarnings("rawtypes")
@RestController
@RequestMapping(path = "/restaurants/{restaurantId}/paymentMethods", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
public class RestaurantPaymentMethodController extends BaseController<PaymentMethod, PaymentMethodDTO, PaymentMethodInput> {
			
	@Autowired
	private RestaurantService restaurantService;
	
	@GetMapping
	public ResponseEntity list(@PathVariable final Long restaurantId) {
		
		final Restaurant restaurant = restaurantService.findById(restaurantId);
		
		return ResponseEntity
				.ok(toCollectionModelDTO(restaurant.getPaymentMethods()));		
		
	}
	
	@DeleteMapping("/{paymentMethodId}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void dissassociatePaymentMethod(@PathVariable final Long restaurantId,
			@PathVariable final Long paymentMethodId) {
		
		restaurantService.dissassociatePaymentMethod(restaurantId, paymentMethodId);
	}
	
	@PutMapping("/{paymentMethodId}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void assossiate(@PathVariable final Long restaurantId,
			@PathVariable final Long paymentMethodId) {
		
		restaurantService.assossiate(restaurantId, paymentMethodId);
	}

}
