package com.algaworks.meat.api.controller;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.algaworks.meat.api.model.dto.UserPermissionDTO;
import com.algaworks.meat.api.model.input.UserPermissionInput;
import com.algaworks.meat.domain.model.UserPermission;
import com.algaworks.meat.domain.service.UserPermissionService;
import com.algaworks.meat.infraestructure.BaseController;

@SuppressWarnings("rawtypes")
@RestController
@RequestMapping(path = "/groups/{groupId}/userPermissions", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
public class GroupUserPermissionsController extends BaseController<UserPermission, UserPermissionDTO, UserPermissionInput> {
	
	@Autowired
	private UserPermissionService userPermissionService;
	
	@GetMapping
	public ResponseEntity list(@PathVariable final Long groupId) {
		
		return ResponseEntity
				.ok(toCollectionModelDTO(userPermissionService.listUserPermissions(groupId)));		
		
	}
	
	@GetMapping("/{userPermissionId}")
	public ResponseEntity findUserPermission(@PathVariable final Long groupId, @PathVariable final Long userPermissionId) {
		
		return ResponseEntity
				.ok(toModelDTO(userPermissionService.findById(groupId,userPermissionId)));		
		
	}
	
	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping()
	public void save(
			@PathVariable final Long groupId, @RequestBody final UserPermissionInput userPermission) {
		
		userPermissionService.persist(groupId, toModel(userPermission));		
			
		
	}
	
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@PutMapping("/{userPermissionId}")
	public void update(
			@PathVariable final Long groupId,
			@PathVariable final Long userPermissionId, 
			@RequestBody final UserPermissionInput userPermissionInput) {
		
		UserPermission userPermission =
				this.userPermissionService.findById(groupId, userPermissionId);	
		
		BeanUtils.copyProperties(userPermissionInput,userPermission,"id");							
		
		userPermissionService.persist(groupId, userPermission);					
		
	}
	
	@PutMapping("desassociate/{userPermissionId}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void dissassociateUserPermission(@PathVariable final Long groupId,
			@PathVariable final Long userPermissionId) {
		
		userPermissionService.dissassociateUserPermission(groupId, userPermissionId);
	}
	
	@PutMapping("/associate/{userPermissionId}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void associateUserPermission(@PathVariable final Long groupId,
			@PathVariable final Long userPermissionId) {
		
		userPermissionService.associateUserPermission(groupId, userPermissionId);
	}

}
