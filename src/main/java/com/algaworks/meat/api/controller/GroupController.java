package com.algaworks.meat.api.controller;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.algaworks.meat.api.model.dto.GroupDTO;
import com.algaworks.meat.api.model.input.GroupInput;
import com.algaworks.meat.domain.model.Group;
import com.algaworks.meat.domain.service.GroupService;
import com.algaworks.meat.infraestructure.BaseController;

@SuppressWarnings("rawtypes")
@RestController
@RequestMapping(path = "/groups", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
public class GroupController extends BaseController<Group, GroupDTO, GroupInput> {
	
	@Autowired
	private GroupService service;		
	
	@GetMapping()	
	public ResponseEntity list(){				
		return ResponseEntity.ok()
				.body(toCollectionModelDTO(this.service.list()));		
	}
	
	@GetMapping("/{id}")
	public ResponseEntity findById(@PathVariable Long id) {							
		return ResponseEntity.ok()
				.body(toModelDTO(this.service.findById(id)));				
	}	
	
	@PostMapping	
	@ResponseStatus(HttpStatus.CREATED)
	public void save(@RequestBody GroupInput groupInput) {		
		this.service.persist(toModel(groupInput));						
	}
	
	@PutMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public void update(@PathVariable Long id, @RequestBody GroupInput groupInput) {			
		Group group = this.service.findById(id);			
		BeanUtils.copyProperties(groupInput, group,"id");		
		this.service.persist(group);			
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Long id) {			
			this.service.delete(id);			
	}

}
