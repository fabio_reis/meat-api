package com.algaworks.meat.api.controller;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.algaworks.meat.api.model.dto.CityDTO;
import com.algaworks.meat.api.model.input.CityInput;
import com.algaworks.meat.domain.model.City;
import com.algaworks.meat.domain.service.CityService;
import com.algaworks.meat.infraestructure.BaseController;

@SuppressWarnings("rawtypes")
@RestController
@RequestMapping(path = "/cities", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
public class CityController extends BaseController<City, CityDTO, CityInput> {
	
	@Autowired
	private CityService service;		
	
	@GetMapping()	
	public ResponseEntity list(){				
		return ResponseEntity.ok()
				.body(toCollectionModelDTO(this.service.list()));		
	}
	
	@GetMapping("/{id}")
	public ResponseEntity findById(@PathVariable Long id) {							
		return ResponseEntity.ok()
				.body(toModelDTO(this.service.findById(id)));				
	}	
	
	@PostMapping	
	@ResponseStatus(HttpStatus.CREATED)
	public void save(@RequestBody CityInput cityInput) {		
		this.service.persist(toModel(cityInput));						
	}
	
	@PutMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public void update(@PathVariable Long id, @RequestBody CityInput cityInput) {			
		City city = this.service.findById(id);			
		BeanUtils.copyProperties(cityInput, city,"id");		
		this.service.persist(city);			
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Long id) {			
			this.service.delete(id);			
	}

}
