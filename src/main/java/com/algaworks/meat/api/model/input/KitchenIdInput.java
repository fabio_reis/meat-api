package com.algaworks.meat.api.model.input;

public class KitchenIdInput {
		
	private Long id;

	public Long getId() {return id;}
	public void setId(Long id) {this.id = id;}		

}
