package com.algaworks.meat.api.model.dto.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.algaworks.meat.api.model.dto.CityDTO;
import com.algaworks.meat.api.model.input.CityInput;
import com.algaworks.meat.domain.model.City;

@Component
public class CityDTOMapper {
	
	@Autowired
	private ModelMapper mapper;
	
	public CityDTO toModelDTO(City city) {		
		return mapper.map(city, CityDTO.class);	
	}
	
	public CityInput toModelInput(City city) {
		return mapper.map(city, CityInput.class);
	}
	
	public City toModel(CityInput input) {
		return mapper.map(input, City.class);
	}
	
	public List<CityDTO> toCollectionModelDTO(List<City> cities){
		return cities.stream()
				.map(city -> toModelDTO(city))
				.collect(Collectors.toList());
	}	

}
