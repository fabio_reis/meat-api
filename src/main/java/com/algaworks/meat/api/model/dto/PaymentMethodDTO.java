package com.algaworks.meat.api.model.dto;

import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName("paymentMethod")
public class PaymentMethodDTO {
	
	private Long id;
	private String description;
	
	public Long getId() {return id;}
	public void setId(Long id) {this.id = id;}
	
	public String getDescription() {return description;}
	public void setDescription(String description) {this.description = description;}	

}
