package com.algaworks.meat.api.model.dto.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.algaworks.meat.api.model.dto.KitchenDTO;
import com.algaworks.meat.api.model.input.KitchenInput;
import com.algaworks.meat.domain.model.Kitchen;

@Component
public class KitchenDTOMapper {
	
	@Autowired
	private ModelMapper mapper;
	
	public KitchenDTO toModelDTO(Kitchen Kitchen) {		
		return mapper.map(Kitchen, KitchenDTO.class);	
	}
	
	public KitchenInput toModelInput(Kitchen kitchen) {
		return mapper.map(kitchen, KitchenInput.class);
	}
	
	public Kitchen toModel(KitchenInput input) {
		return mapper.map(input, Kitchen.class);
	}
	
	public List<KitchenDTO> toCollectionModelDTO(List<Kitchen> kitchens){
		return kitchens.stream()
				.map(kitchen -> toModelDTO(kitchen))
				.collect(Collectors.toList());
	}	

}
