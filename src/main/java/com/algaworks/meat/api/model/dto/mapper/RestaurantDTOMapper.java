package com.algaworks.meat.api.model.dto.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.algaworks.meat.api.model.dto.RestaurantDTO;
import com.algaworks.meat.api.model.input.RestaurantInput;
import com.algaworks.meat.domain.model.Restaurant;

@Component
public class RestaurantDTOMapper {
	
	@Autowired
	private ModelMapper mapper;
	
	public RestaurantDTO toModelDTO(Restaurant restaurant) {		
		return mapper.map(restaurant, RestaurantDTO.class);	
	}
	
	public RestaurantInput toModelInput(Restaurant restaurant) {
		return mapper.map(restaurant, RestaurantInput.class);
	}
	
	public Restaurant toModel(RestaurantInput input) {
		return mapper.map(input, Restaurant.class);
	}
	
	public List<RestaurantDTO> toCollectionModelDTO(List<Restaurant> restaurants){
		return restaurants.stream()
				.map(restaurant -> toModelDTO(restaurant))
				.collect(Collectors.toList());
	}	

}
