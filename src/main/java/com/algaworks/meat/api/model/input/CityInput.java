package com.algaworks.meat.api.model.input;

public class CityInput {
	
	private Long id;
	private String name;
	private UFIdInput ufId;
	
	public Long getId() {return id;}
	public void setId(Long id) {this.id = id;}
	
	public String getName() {return name;}
	public void setName(String name) {this.name = name;}
	
	public UFIdInput getUfId() {return ufId;}
	public void setUfId(UFIdInput ufId) {this.ufId = ufId;}	

}
