package com.algaworks.meat.api.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class AddressDTO {	
	
	private String postalCode;	
	
	private String street;	
	
	private String number;	
	
	private String complement;	

	private String district;	
	
	private CityDTO city;

	public String getPostalCode() {return postalCode;}
	public void setPostalCode(String postalCode) {this.postalCode = postalCode;}

	public String getStreet() {return street;}
	public void setStreet(String street) {this.street = street;}

	public String getNumber() {return number;}
	public void setNumber(String number) {this.number = number;}

	public String getComplement() {return complement;}
	public void setComplement(String complement) {this.complement = complement;}

	public String getDistrict() {return district;}
	
	public void setDistrict(String district) {this.district = district;}
	
	public CityDTO getCity() {return city;}
	public void setCity(CityDTO city) {this.city = city;}

}
