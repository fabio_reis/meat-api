package com.algaworks.meat.api.model.dto.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.algaworks.meat.api.model.dto.UFDTO;
import com.algaworks.meat.api.model.input.UFInput;
import com.algaworks.meat.domain.model.UF;

@Component
public class UFDTOMapper {
	
	@Autowired
	private ModelMapper mapper;
	
	public UFDTO toModelDTO(UF uf) {		
		return mapper.map(uf, UFDTO.class);	
	}
	
	public UFInput toModelInput(UF uf) {
		return mapper.map(uf, UFInput.class);
	}
	
	public UF toModel(UFInput input) {
		return mapper.map(input, UF.class);
	}
	
	public List<UFDTO> toCollectionModelDTO(List<UF> UFs){
		return UFs.stream()
				.map(uf -> toModelDTO(uf))
				.collect(Collectors.toList());
	}	

}
