package com.algaworks.meat.api.model.input;

public class ProductInput {	
	
	private String name;
	private String description;
	private RestaurantIdInput restaurant;
	
	
	public String getName() {return name;}
	public void setName(String name) {this.name = name;}
	
	public String getDescription() {return description;}
	public void setDescription(String description) {this.description = description;}
	
	public RestaurantIdInput getRestaurant() {return restaurant;}
	public void setRestaurant(RestaurantIdInput restaurant) {this.restaurant = restaurant;}	

}
