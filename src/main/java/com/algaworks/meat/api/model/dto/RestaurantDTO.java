package com.algaworks.meat.api.model.dto;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName("restaurant")
public class RestaurantDTO {
	
	private Long id;
	private Boolean active;
	private String name;
	private BigDecimal shipping;		
	private KitchenDTO kitchen;	
	private AddressDTO address;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.XXX")
	private OffsetDateTime dtCreation;
	
	public Long getId() {return id;}
	public void setId(Long id) {this.id = id;}		
	
	public Boolean getActive() {return active;}
	public void setActive(Boolean active) {this.active = active;}
	
	public String getName() {return name;}
	public void setName(String name) {this.name = name;}
	
	public BigDecimal getShipping() {return shipping;}
	public void setShipping(BigDecimal shipping) {this.shipping = shipping;}
	
	public KitchenDTO getKitchen() {return kitchen;}
	public void setKitchen(KitchenDTO kitchen) {this.kitchen = kitchen;}
	
	public OffsetDateTime getDtCreation() {return dtCreation;}
	public void setDtCreation(OffsetDateTime dtCreation) {this.dtCreation = dtCreation;}
	
	public AddressDTO getAddress() {return address;}
	public void setAddress(AddressDTO address) {this.address = address;}	
	
}
