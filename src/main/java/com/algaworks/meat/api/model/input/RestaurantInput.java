package com.algaworks.meat.api.model.input;

import java.math.BigDecimal;


public class RestaurantInput {
	
	private Long id;
	private String name;
	private BigDecimal shipping;	
	private KitchenIdInput kitchenId;		
	private AddressInput address;
	
	public Long getId() {return id;}
	public void setId(Long id) {this.id = id;}
	
	public String getName() {return name;}
	public void setName(String name) {this.name = name;}
	
	public BigDecimal getShipping() {return shipping;}
	public void setShipping(BigDecimal shipping) {this.shipping = shipping;}	
	
	public KitchenIdInput getKitchenId() {return kitchenId;}
	public void setKitchenId(KitchenIdInput kitchenId) {this.kitchenId = kitchenId;}
	
	public AddressInput getAddress() {return address;}
	public void setAddress(AddressInput address) {this.address = address;}
	

}
