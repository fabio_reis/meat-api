package com.algaworks.meat.api.model.input;

public class PaymentMethodInput {
	
	private String description;

	public String getDescription() {return description;}
	public void setDescription(String description) {this.description = description;}	

}
