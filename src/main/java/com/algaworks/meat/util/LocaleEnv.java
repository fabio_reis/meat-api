package com.algaworks.meat.util;

import java.util.Locale;

import org.springframework.stereotype.Component;

@Component
public class LocaleEnv {			
		
	public Locale get() {		
		return new Locale(System.getenv("LOCALE"));
	}
	
}