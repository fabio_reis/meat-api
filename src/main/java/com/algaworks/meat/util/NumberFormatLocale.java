package com.algaworks.meat.util;

import java.text.NumberFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class NumberFormatLocale {
		
	@Autowired
	private LocaleEnv localeEnv;
	
	public String toNumber(Number number) {
		return NumberFormat.getInstance(localeEnv.get())
				.format(number);		
		
	}
	
	public String toCurrency(String currency) {
		return NumberFormat.getCurrencyInstance(localeEnv.get())
				.format(currency);		
		
	}	

}
