package com.algaworks.meat.util;

import java.text.DateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DateFormatLocale {	
	
	@Autowired
	private LocaleEnv localeEnv;	
	
	public String format(Date date) {
		return DateFormat.getDateInstance(DateFormat.MEDIUM, localeEnv.get())
				.format(date);
		
	}

}
