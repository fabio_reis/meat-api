package com.algaworks.meat.config;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource("classpath:queries/*_queries.xml")
public class SpringBootXmlBeanConfig {

	

}
