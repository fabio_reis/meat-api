package com.algaworks.meat.domain.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

import com.algaworks.meat.domain.enums.OrderStatus;

@Entity
@Table(name = "pedido")
public class Order {
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "sub_total", nullable = false)
	private BigDecimal subTotal;
	
	@Column(name = "taxa_frete", nullable = false)
	private BigDecimal shipping;
	
	@Column(name = "valor_total", nullable = false)
	private BigDecimal totalValue;
	
	@CreationTimestamp
	@Column(name = "data_criacao", nullable = false)
	private LocalDateTime  dtCreation;
	
	@Column(name = "data_confirmacao")
	private LocalDateTime  dtConfirm;
	
	@Column(name = "data_cancelamento")
	private LocalDateTime  dtCancel;
	
	@Column(name = "data_entrega")
	private LocalDateTime  dtDelivery;
	
	@Enumerated(EnumType.ORDINAL)
	@Column(nullable = false)
	private OrderStatus status;
	
	@Embedded
	private Address address;
	
	@ManyToOne
	@JoinColumn(name = "forma_pagamento_id", nullable = false)
	private PaymentMethod paymentMethod;
	
	@ManyToOne
	@JoinColumn(name = "usuario_id", nullable = false)
	private User user;	
	
	@ManyToOne
	@JoinColumn(name = "restaurante_id", nullable = false)
	private Restaurant restaurant;
	
	@OneToMany
	@JoinColumn(name = "item_id")
	private List<OrderItem> items = new ArrayList<>();

	public Long getId() {return id;}
	public void setId(Long id) {this.id = id;}

	public BigDecimal getSubTotal() {return subTotal;}
	public void setSubTotal(BigDecimal subTotal) {this.subTotal = subTotal;}

	public BigDecimal getShipping() {return shipping;}
	public void setShipping(BigDecimal shipping) {this.shipping = shipping;}

	public BigDecimal getTotalValue() {return totalValue;}
	public void setTotalValue(BigDecimal totalValue) {this.totalValue = totalValue;}

	public LocalDateTime  getDtCreation() {return dtCreation;}
	public void setDtCreation(LocalDateTime  dtCreation) {this.dtCreation = dtCreation;}

	public LocalDateTime  getDtConfirm() {return dtConfirm;}
	public void setDtConfirm(LocalDateTime  dtConfirm) {this.dtConfirm = dtConfirm;}

	public LocalDateTime  getDtCancel() {return dtCancel;}
	public void setDtCancel(LocalDateTime  dtCancel) {this.dtCancel = dtCancel;}

	public LocalDateTime  getDtDelivery() {return dtDelivery;}
	public void setDtDelivery(LocalDateTime  dtDelivery) {this.dtDelivery = dtDelivery;}		
	
	public OrderStatus getStatus() {return status;}
	public void setStatus(OrderStatus status) {this.status = status;}		
	
	public Address getAddress() {return address;}
	public void setAddress(Address address) {this.address = address;}	
	
	public Restaurant getRestaurant() {return restaurant;}
	public void setRestaurant(Restaurant restaurant) {this.restaurant = restaurant;}
	
	public PaymentMethod getPaymentMethod() {return paymentMethod;}
	public void setPaymentMethod(PaymentMethod paymentMethod) {this.paymentMethod = paymentMethod;}	
	
	public User getUser() {return user;}
	public void setUser(User user) {this.user = user;}
	
	public List<OrderItem> getItems() {return items;}
	public void setItems(List<OrderItem> items) {this.items = items;}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Order other = (Order) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}	

}
