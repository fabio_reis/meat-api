package com.algaworks.meat.domain.model;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name = "restaurante")
public class Restaurant {
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)	
	private Long id;	
	
	@Column(name = "nome", nullable = false)
	private String name;
	
	@Column(name="taxa_frete", nullable = false)
	private BigDecimal shipping;		
	
	@ManyToOne
	@JoinColumn(name = "cozinha_id", nullable = false)
	private Kitchen kitchen;
	
	@CreationTimestamp
	@Column(name = "data_cadastro", nullable = false)
	private OffsetDateTime dtCreation;
	
	@UpdateTimestamp
	@Column(name = "data_atualizacao")
	private OffsetDateTime dtUpdate;
	
	@Embedded
	private Address address;
	
	@Column(name = "ativo", nullable = false)
	private Boolean active;
	
	@OneToMany(mappedBy = "restaurant")	
	private List<Product> products = new ArrayList<>();
	
	@ManyToMany()
	@JoinTable(name = "restaurante_forma_pagamento",
		joinColumns = @JoinColumn(name="restaurante_id"),
		inverseJoinColumns = @JoinColumn(name="forma_pagamento_id"))
	private List<PaymentMethod> paymentMethods = new ArrayList<>();	
	
	@ManyToMany()
	@JoinTable(name = "restaurante_usuario_responsavel",
		joinColumns = @JoinColumn(name="restaurante_id"),
		inverseJoinColumns = @JoinColumn(name="usuario_id"))
	private List<User> users = new ArrayList<>();

	public Long getId() {return id;}
	public void setId(Long id) {this.id = id;}

	public Kitchen getKitchen() {return kitchen;}
	public void setKitchen(Kitchen kitchen) {this.kitchen = kitchen;}

	public String getName() {return name;}
	public void setName(String name) {this.name = name;}

	public BigDecimal getShipping() {return shipping;}
	public void setShipping(BigDecimal shipping) {this.shipping = shipping;}		
	
	public List<PaymentMethod> getPaymentMethods() {return paymentMethods;}
	public void setPaymentMethods(List<PaymentMethod> paymentMethods) {this.paymentMethods = paymentMethods;}	
	
	public Address getAddress() {return address;}
	public void setAddress(Address address) {this.address = address;}			
	
	public OffsetDateTime getDtCreation() {return dtCreation;}
	public void setDtCreation(OffsetDateTime dtCreation) {this.dtCreation = dtCreation;}
	
	public OffsetDateTime getDtUpdate() {return dtUpdate;}
	public void setDtUpdate(OffsetDateTime dtUpdate) {this.dtUpdate = dtUpdate;}
	
	public List<Product> getProducts() {return products;}
	public void setProducts(List<Product> products) {this.products = products;}
	
	public Boolean getActive() {return active;}
	public void setActive(Boolean active) {this.active = active;}	
	
	public List<User> getUsers() {return users;}
	public void setUsers(List<User> users) {this.users = users;}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Restaurant other = (Restaurant) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
