package com.algaworks.meat.domain.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="produto")
public class Product {
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "nome")
	private String name;
	
	@Column(name = "descricao")
	private String description;
	
	@Column(name = "preco")	
	private BigDecimal amount;
	
	@Column(name = "ativo")
	private Boolean active;
	
	@ManyToOne
	@JoinColumn(name = "restaurant_id")
	private Restaurant restaurant;

	public Long getId() {return id;}
	public void setId(Long id) {this.id = id;}	

	public String getName() {return name;}
	public void setName(String name) {this.name = name;}
	
	public String getDescription() {return description;}
	public void setDescription(String description) {this.description = description;}

	public BigDecimal getAmount() {return amount;}
	public void setAmount(BigDecimal amount) {this.amount = amount;}

	public Boolean getActive() {return active;}
	public void setActive(Boolean active) {this.active = active;}	
	
	public Restaurant getRestaurant() {return restaurant;}
	public void setRestaurant(Restaurant restaurant) {this.restaurant = restaurant;}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
