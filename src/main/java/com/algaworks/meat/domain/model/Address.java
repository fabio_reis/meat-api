package com.algaworks.meat.domain.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Embeddable
public class Address {	
	
	@Column(name = "endereco_cep")
	private String postalCode;
	
	@Column(name = "endereco_logradouro")
	private String street;
	
	@Column(name = "endereco_numero")
	private String number;
	
	@Column(name = "endereco_complemento")
	private String complement;
	
	@Column(name = "endereco_bairro")
	private String district;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="endereco_cidade_id")
	private City city;
	
	public String getPostalCode() {return postalCode;}
	public void setPostalCode(String postalCode) {this.postalCode = postalCode;}
	
	public String getStreet() {return street;}
	public void setStreet(String street) {this.street = street;}
	
	public String getNumber() {return number;}
	public void setNumber(String number) {this.number = number;}
	
	public String getComplement() {return complement;}
	public void setComplement(String complement) {this.complement = complement;}
	
	public String getDistrict() {return district;}
	public void setDistrict(String district) {this.district = district;}
	
	public City getCity() {return city;}
	public void setCity(City city) {this.city = city;}	

}
