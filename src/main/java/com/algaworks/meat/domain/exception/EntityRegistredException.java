package com.algaworks.meat.domain.exception;

public class EntityRegistredException extends BussinessException {

	private static final long serialVersionUID = 1L;
	
	public EntityRegistredException(String message) {
		super(message);
	}

}
