package com.algaworks.meat.domain.exception;

public class EmailRegisteredException extends BussinessException {
	
	private static final long serialVersionUID = 1L;
	
	public EmailRegisteredException(String message) {
		super(message);		
		
	}	

}
