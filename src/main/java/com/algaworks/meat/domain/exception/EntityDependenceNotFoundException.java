package com.algaworks.meat.domain.exception;

public class EntityDependenceNotFoundException extends BussinessException {	
	
	private static final long serialVersionUID = 1L;

	public EntityDependenceNotFoundException(String message) {
		super(message);
	}

}
