package com.algaworks.meat.domain.exception;

public class EmailInvalidException extends BussinessException {
	
	private static final long serialVersionUID = 1L;
	
	public EmailInvalidException(String message) {
		super(message);		
	}

}
