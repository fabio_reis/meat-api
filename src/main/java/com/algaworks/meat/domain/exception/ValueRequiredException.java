package com.algaworks.meat.domain.exception;

public class ValueRequiredException extends BussinessException {	
	
	private static final long serialVersionUID = 1L;

	public ValueRequiredException(String message) {
		super(message);
	}

}
