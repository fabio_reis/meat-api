package com.algaworks.meat.domain.exception;

public class EntityInUseException extends BussinessException {
	
	private static final long serialVersionUID = 1L;
	
	public EntityInUseException(String message) {
		super(message);
	}

}
