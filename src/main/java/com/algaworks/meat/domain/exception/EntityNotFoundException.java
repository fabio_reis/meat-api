package com.algaworks.meat.domain.exception;

public class EntityNotFoundException extends BussinessException {	
	
	private static final long serialVersionUID = 1L;

	public EntityNotFoundException(String message) {
		super(message);
	}

}
