package com.algaworks.meat.domain.exception;

public class PasswordException extends BussinessException{
	
	private static final long serialVersionUID = 1L;

	public PasswordException(String message) {
		super(message);		
	}

}
