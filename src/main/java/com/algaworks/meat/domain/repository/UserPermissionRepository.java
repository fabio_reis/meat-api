package com.algaworks.meat.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.algaworks.meat.domain.model.UserPermission;

@Repository
public interface UserPermissionRepository extends JpaRepository<UserPermission, Long> {

}
