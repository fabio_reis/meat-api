package com.algaworks.meat.domain.repository;

import org.springframework.stereotype.Repository;

import com.algaworks.meat.domain.model.UF;
import com.algaworks.meat.infraestructure.CustomRepository;

@Repository
public interface UFRepository extends CustomRepository<UF, Long> {	
	
	boolean existsByName(String name);	

}
