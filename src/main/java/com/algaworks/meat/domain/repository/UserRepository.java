package com.algaworks.meat.domain.repository;

import java.util.Optional;

import com.algaworks.meat.domain.model.User;
import com.algaworks.meat.infraestructure.CustomRepository;

public interface UserRepository extends CustomRepository<User, Long> {	
	
	Optional<User> findByEmail(String email);

}
