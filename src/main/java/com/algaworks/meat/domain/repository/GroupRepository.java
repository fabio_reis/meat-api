package com.algaworks.meat.domain.repository;

import java.util.Optional;
import org.springframework.stereotype.Repository;
import com.algaworks.meat.domain.model.Group;
import com.algaworks.meat.infraestructure.CustomRepository;

@Repository
public interface GroupRepository extends CustomRepository<Group, Long>{
	
	Optional<Group> findByName(String name);	

}
