package com.algaworks.meat.domain.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.algaworks.meat.domain.model.Restaurant;
import com.algaworks.meat.infraestructure.CustomRepository;



@Repository
public interface RestaurantRepository extends CustomRepository<Restaurant, Long> {		
	
	@Query("from Restaurant r join fetch r.kitchen")
	List<Restaurant> findAll();		

}
