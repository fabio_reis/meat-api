package com.algaworks.meat.domain.repository;

import org.springframework.stereotype.Repository;

import com.algaworks.meat.domain.model.City;
import com.algaworks.meat.infraestructure.CustomRepository;

@Repository
public interface CityRepository extends CustomRepository<City, Long> {	
	
	 boolean existsByName(String name);	

}
