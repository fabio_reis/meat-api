package com.algaworks.meat.domain.repository;

import org.springframework.stereotype.Repository;

import com.algaworks.meat.domain.model.PaymentMethod;
import com.algaworks.meat.infraestructure.CustomRepository;

@Repository
public interface PaymentMethodRepository extends CustomRepository<PaymentMethod, Long> {	
	
	 boolean existsByDescription(String description);		 
	

}
