package com.algaworks.meat.domain.repository;

import com.algaworks.meat.domain.model.Product;
import com.algaworks.meat.infraestructure.CustomRepository;

public interface ProductRepository extends CustomRepository<Product, Long> {

}
