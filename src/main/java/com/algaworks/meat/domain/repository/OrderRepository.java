package com.algaworks.meat.domain.repository;

import com.algaworks.meat.domain.model.Order;
import com.algaworks.meat.infraestructure.CustomRepository;

public interface OrderRepository extends CustomRepository<Order, Long> {

}
