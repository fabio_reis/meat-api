package com.algaworks.meat.domain.repository;

import org.springframework.stereotype.Repository;

import com.algaworks.meat.domain.model.Kitchen;
import com.algaworks.meat.infraestructure.CustomRepository;


@Repository
public interface KitchenRepository extends CustomRepository<Kitchen, Long> {	
	
	boolean existsByName(String name);	
	
}
