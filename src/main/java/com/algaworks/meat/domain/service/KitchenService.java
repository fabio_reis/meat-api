package com.algaworks.meat.domain.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.algaworks.meat.domain.exception.EntityInUseException;
import com.algaworks.meat.domain.exception.EntityNotFoundException;
import com.algaworks.meat.domain.exception.EntityRegistredException;
import com.algaworks.meat.domain.exception.ValueRequiredException;
import com.algaworks.meat.domain.model.Kitchen;
import com.algaworks.meat.domain.repository.KitchenRepository;

@Service
public class KitchenService {	
	
	private static final String MSG_KITCHEN_NOT_FOUND = "Cozinha de código %d não encontrada";
	
	@Autowired
	private KitchenRepository repository;	
	
	public List<Kitchen> list(){		
		return this.repository.findAll();				
	}
	
	public Kitchen findById(Long id) {		
		return this.repository.findById(id).orElseThrow(
				() -> new EntityNotFoundException(String.format(MSG_KITCHEN_NOT_FOUND,id)));		
	}	
	
	public void persist(Kitchen kitchen) {
		
		if(ObjectUtils.isEmpty(kitchen.getName()))
			throw new ValueRequiredException("O campo nome é obrigatório");
		
		if(kitchen.getId() == null &&
				repository.existsByName(kitchen.getName()))
			throw new EntityRegistredException(String.format("Cozinha de nome %s já cadastrada", kitchen.getName()));
		
		this.repository.save(kitchen);
	}	
	
	public void delete(Long id) {
		try {		
			
			this.repository.deleteById(id);
			
		}catch(EmptyResultDataAccessException e) {
			throw new EntityNotFoundException(String.format(MSG_KITCHEN_NOT_FOUND,id));
			
		}catch (DataIntegrityViolationException ex) {
			throw new EntityInUseException(String.format("Cozinha de código %d está em uso",id));
		}
	}
}
