package com.algaworks.meat.domain.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.algaworks.meat.domain.exception.EmailRegisteredException;
import com.algaworks.meat.domain.exception.EntityInUseException;
import com.algaworks.meat.domain.exception.EntityNotFoundException;
import com.algaworks.meat.domain.exception.ValueRequiredException;
import com.algaworks.meat.domain.model.Group;
import com.algaworks.meat.domain.model.UserPermission;
import com.algaworks.meat.domain.repository.GroupRepository;

@Service
public class GroupService {
	
	private static final String MSG_GROUP_NOT_FOUND = "Grupo de id %d não encontrada";

	@Autowired
	private GroupRepository repository;	
	
	
	public Group findById(Long id) {
		return this.repository.findById(id).orElseThrow(
				() -> new EntityNotFoundException(String.format(MSG_GROUP_NOT_FOUND, id)));
	}
	
	public List<UserPermission> listUserPermissions(Long groupId) {
		Group group = findById(groupId);
		return group.getPermissions();
	}
	
	public List<Group> list() {
		return this.repository.findAll();
	}	
	
	public void persist(Group group) {		
			
		if(ObjectUtils.isEmpty(group.getName()))
			throw new ValueRequiredException("O campo nome é obrigatório");	
		
		final Group groupByName = repository.findByName(group.getName()).orElse(null);
		
		if(!ObjectUtils.isEmpty(groupByName) && group.getId() != groupByName.getId())
			throw new EmailRegisteredException("Grupo já cadastrado");
		
		this.repository.save(group);		
		
		
	}	
	
	public void delete(Long id) {		
		try {		
			
			this.repository.deleteById(id);
			
		}catch(EmptyResultDataAccessException e) {
			throw new EntityNotFoundException(String.format("Grupo de código %d não encontrada",id));
			
		}catch (DataIntegrityViolationException ex) {
			throw new EntityInUseException(String.format("Grupo de código %d está em uso",id));
		}
	}

}
