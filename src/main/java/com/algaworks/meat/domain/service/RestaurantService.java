package com.algaworks.meat.domain.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import com.algaworks.meat.domain.exception.EntityDependenceNotFoundException;
import com.algaworks.meat.domain.exception.EntityInUseException;
import com.algaworks.meat.domain.exception.EntityNotFoundException;
import com.algaworks.meat.domain.exception.ValueRequiredException;
import com.algaworks.meat.domain.model.PaymentMethod;
import com.algaworks.meat.domain.model.Restaurant;
import com.algaworks.meat.domain.model.User;
import com.algaworks.meat.domain.repository.RestaurantRepository;

@Service
public class RestaurantService {	
	
	@Autowired
	private RestaurantRepository repository;
	
	@Autowired
	private KitchenService kitchenService;	
	
	@Autowired
	private UFService ufService;
	
	@Autowired
	private CityService cityService;
	
	@Autowired
	private PaymentMethodService paymentMethodService;
	
	public List<Restaurant> list(){		
		return this.repository.findAll();				
	}
	
	public Restaurant findById(Long id) {		
		return this.repository.findById(id).orElseThrow(				
				() -> new EntityNotFoundException(String.format("Restaurante de código %d não encontrado",id)));		
	}	

	public void persist(Restaurant restaurant) {	
		
		try {
			
			if(ObjectUtils.isEmpty(restaurant.getName()))
				throw new ValueRequiredException("O campo nome do restaurant é obrigatório");
			
			if(ObjectUtils.isEmpty(restaurant.getShipping()) || restaurant.getShipping().compareTo(BigDecimal.ZERO) < 0)
				throw new ValueRequiredException("O campo frete é obrigatório");
			
			if(ObjectUtils.isEmpty(restaurant.getKitchen()) || ObjectUtils.isEmpty(restaurant.getKitchen().getId()))
				throw new ValueRequiredException("O campo Cozinha é obrigatorio");
			
			if(ObjectUtils.isEmpty(restaurant.getAddress()))
				throw new ValueRequiredException("O Endereço é obrigatório");
			
			if(ObjectUtils.isEmpty(restaurant.getAddress().getStreet()))
				throw new ValueRequiredException("O campo Rua é obrigatório");
			
			if(ObjectUtils.isEmpty(restaurant.getAddress().getPostalCode()))
				throw new ValueRequiredException("O campo Cep é obrigatório");
			
			if(ObjectUtils.isEmpty(restaurant.getAddress().getDistrict()))
				throw new ValueRequiredException("O campo distrito é obrigatório");				
			
			if(ObjectUtils.isEmpty(restaurant.getAddress().getCity()))
				throw new ValueRequiredException("O campo Cidade é obrigatório");
			
			if(ObjectUtils.isEmpty(restaurant.getAddress().getCity().getName()))
				throw new ValueRequiredException("O campo nome da Cidade é obrigatório");
			
			if(ObjectUtils.isEmpty(restaurant.getAddress().getCity().getUf()))
				throw new ValueRequiredException("O campo Estado é obrigatório");
			
			if(ObjectUtils.isEmpty(restaurant.getAddress().getCity().getUf().getId()))
				throw new ValueRequiredException("O campo id do estado é obrigatório");
			
			kitchenService.findById(restaurant.getKitchen().getId());
			
			cityService.findById(restaurant.getAddress().getCity().getId());
			
			ufService.findById(restaurant.getAddress().getCity().getUf().getId());
			
			this.repository.save(restaurant);
			
		}catch(EntityNotFoundException e) {
			throw new EntityDependenceNotFoundException(e.getMessage());
		}
				
	}
	
	public void inactivate(Long id) {
		Restaurant restautant = this.findById(id);
		restautant.setActive(false);
				
	}
	
	public void activate(Long id) {
		Restaurant restautant = this.findById(id);
		restautant.setActive(true);
				
	}
	
	public void delete(Long id) {
		try {		
			
			this.repository.deleteById(id);
			
		}catch(EmptyResultDataAccessException e) {
			throw new EntityNotFoundException(String.format("Restaurante de código %d não encontrada",id));
			
		}catch (DataIntegrityViolationException ex) {
			throw new EntityInUseException(String.format("Restaurante de código %d está em uso",id));
		}
	}
	
	@Transactional
	public void dissassociatePaymentMethod(Long restaurantId, Long paymentMethodId) {
		Restaurant restaurant = findById(restaurantId);
		PaymentMethod paymentMethod = paymentMethodService.findById(paymentMethodId);
		restaurant.getPaymentMethods().remove(paymentMethod);
	}	
	
	@Transactional
	public void assossiate(Long restaurantId, Long paymentMethodId) {
		List<PaymentMethod> restaurantPaumentMethods = findById(restaurantId).getPaymentMethods();
		PaymentMethod paymentMethod = paymentMethodService.findById(paymentMethodId);
		
		if(!restaurantPaumentMethods.contains(paymentMethod))
			restaurantPaumentMethods.add(paymentMethod);
		
	}	

}
