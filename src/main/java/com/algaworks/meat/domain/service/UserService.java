package com.algaworks.meat.domain.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.algaworks.meat.domain.exception.EmailRegisteredException;
import com.algaworks.meat.domain.exception.EntityNotFoundException;
import com.algaworks.meat.domain.exception.PasswordException;
import com.algaworks.meat.domain.exception.ValueRequiredException;
import com.algaworks.meat.domain.model.User;
import com.algaworks.meat.domain.repository.UserRepository;
import com.algaworks.meat.util.Validation;

@Service
public class UserService {	
	
	private static final String MSG_USER_NOT_FOUND = "Usuário de código %d não encontrado";
	
	@Autowired
	private UserRepository repository;
	
	
	public User findById(Long id) {
		return this.repository.findById(id).orElseThrow(
				() -> new EntityNotFoundException(String.format(MSG_USER_NOT_FOUND, id)));
	}
	
	public List<User> list() {
		return this.repository.findAll();
	}	
	
	public void persist(User user) {	
		
		if(ObjectUtils.isEmpty(user.getName()))
			throw new ValueRequiredException("O campo nome é obrigatório");
			
		if(ObjectUtils.isEmpty(user.getEmail()))
			throw new ValueRequiredException("O campo email é obrigatório");
		
		if(ObjectUtils.isEmpty(user.getPassword()))
			throw new ValueRequiredException("O campo password é obrigatório");				
		
		if(!Validation.email(user.getEmail()))
			throw new EmailRegisteredException("Email inválido");		
		
		
		final User userByEmail = repository.findByEmail(user.getEmail()).orElse(null);
		
		if(!ObjectUtils.isEmpty(userByEmail) && user.getId() != userByEmail.getId())
			throw new EmailRegisteredException("Email já cadastrado");
		
		this.repository.save(user);		
	}	
	
	public void updatePassword(User user) {
		
		if(ObjectUtils.isEmpty(user.getPassword()))
			throw new ValueRequiredException("O campo password é obrigatório");
		
		if(ObjectUtils.isEmpty(user.getNewPassword()))
			throw new ValueRequiredException("O campo new password é obrigatório");
		
		if(!user.getNewPassword().equals(user.getPassword()))
			throw new PasswordException("Os passwords são diferentes");
	}
	
	public void delete(Long id) {		
		try {		
			
			this.repository.deleteById(id);
			
		}catch(EmptyResultDataAccessException e) {
			throw new EntityNotFoundException(String.format(MSG_USER_NOT_FOUND,id));
			
		}
	}

}
