package com.algaworks.meat.domain.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.algaworks.meat.domain.exception.EntityInUseException;
import com.algaworks.meat.domain.exception.EntityNotFoundException;
import com.algaworks.meat.domain.exception.EntityRegistredException;
import com.algaworks.meat.domain.exception.ValueRequiredException;
import com.algaworks.meat.domain.model.UF;
import com.algaworks.meat.domain.repository.UFRepository;

@Service
public class UFService {
	
	private static final String MSG_UF_NOT_FOUND = "UF de código %d não encontrado";
	
	@Autowired
	private UFRepository repository;
	
	public UF findById(Long id) {
		return this.repository.findById(id).orElseThrow(
				() -> new EntityNotFoundException(String.format(MSG_UF_NOT_FOUND,id)));
	}
	
	public List<UF> list() {
		return this.repository.findAll();
	}	
	
	public void persist(UF uf) {
		
		if(ObjectUtils.isEmpty(uf.getName()))
			throw new ValueRequiredException("O campo nome é obrigatório");
		
		if(repository.existsByName(uf.getName()))
			throw new EntityRegistredException(String.format("UF de nome %s já cadastrado", uf.getName()));
		
		this.repository.save(uf);		
	}	
	
	public void delete(Long id) {
		try {		
			
			this.repository.deleteById(id);
			
		}catch(EmptyResultDataAccessException e) {
			throw new EntityNotFoundException(String.format(MSG_UF_NOT_FOUND,id));
			
		}catch (DataIntegrityViolationException ex) {
			throw new EntityInUseException(String.format("UF de código %d está em uso",id));
		}
	}
}
