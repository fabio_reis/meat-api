package com.algaworks.meat.domain.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.algaworks.meat.domain.enums.OrderStatus;
import com.algaworks.meat.domain.exception.EntityDependenceNotFoundException;
import com.algaworks.meat.domain.exception.EntityInUseException;
import com.algaworks.meat.domain.exception.EntityNotFoundException;
import com.algaworks.meat.domain.exception.ValueRequiredException;
import com.algaworks.meat.domain.model.Order;
import com.algaworks.meat.domain.repository.OrderRepository;

@Service
public class OrderService {	
	
	@Autowired
	private OrderRepository repository;	
	
	@Autowired
	private RestaurantService restaurantService;
	
	@Autowired
	private PaymentMethodService paymentMethodService;
	
	@Autowired
	private UserService userService;
	
	public List<Order> list(){		
		return this.repository.findAll();				
	}
	
	public Order findById(Long id) {		
		return this.repository.findById(id).orElseThrow(				
				() -> new EntityNotFoundException(String.format("Pedido de código %d não encontrado",id)));		
	}	

	public void persist(Order order) {		
		
		try {
			
			if(ObjectUtils.isEmpty(order.getSubTotal()))
				throw new ValueRequiredException("O campo subTotal é obrigatório");
			
			if(ObjectUtils.isEmpty(order.getShipping()))
				throw new ValueRequiredException("O campo frete é obrigatório");
				
			if(ObjectUtils.isEmpty(order.getTotalValue()))
				throw new ValueRequiredException("O campo valor total é obrigatório");
			
			if(ObjectUtils.isEmpty(order.getAddress()))
				throw new ValueRequiredException("O Endereço é obrigatório");
			
			if(ObjectUtils.isEmpty(order.getAddress().getDistrict()))
				throw new ValueRequiredException("O campo bairro é obrigatório");
			
			if(ObjectUtils.isEmpty(order.getAddress().getPostalCode()))
				throw new ValueRequiredException("O campo cep é obrigatório");
			
			if(ObjectUtils.isEmpty(order.getAddress().getStreet()))
				throw new ValueRequiredException("O campo logradouro é obrigatório");
			
			if(ObjectUtils.isEmpty(order.getPaymentMethod()))
				throw new ValueRequiredException("O campo forma de pagamento é obrigatório");
			
			if(ObjectUtils.isEmpty(order.getUser()))
				throw new ValueRequiredException("O campo cliente é obrigatório");
			
			if(ObjectUtils.isEmpty(order.getRestaurant()))
				throw new ValueRequiredException("O campo restaurante é obrigatório");
			
			if(ObjectUtils.isEmpty(order.getItems()))
				throw new ValueRequiredException("O campo itens é obrigatório");				
			
			paymentMethodService.findById(order.getPaymentMethod().getId());
			
			userService.findById(order.getUser().getId());
			
			restaurantService.findById(order.getRestaurant().getId());
			
			order.setStatus(OrderStatus.CREATED);
			
			this.repository.save(order);					
			
		}catch(EntityNotFoundException e) {			
			throw new EntityDependenceNotFoundException(e.getMessage()); 						
		}		
	}	
	
	public void delete(Long id) {
		try {		
			
			this.repository.deleteById(id);
			
		}catch(EmptyResultDataAccessException e) {
			throw new EntityNotFoundException(String.format("Pedido de código %d não encontrada",id));
			
		}catch (DataIntegrityViolationException ex) {
			throw new EntityInUseException(String.format("Pedido de código %d está em uso",id));
		}
	}

}
