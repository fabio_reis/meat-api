package com.algaworks.meat.domain.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.algaworks.meat.domain.exception.EntityDependenceNotFoundException;
import com.algaworks.meat.domain.exception.EntityInUseException;
import com.algaworks.meat.domain.exception.EntityNotFoundException;
import com.algaworks.meat.domain.exception.EntityRegistredException;
import com.algaworks.meat.domain.exception.ValueRequiredException;
import com.algaworks.meat.domain.model.City;
import com.algaworks.meat.domain.repository.CityRepository;

@Service
public class CityService {
	
	private static final String MSG_CITY_NOT_FOUND = "Cidade de id %d não encontrada";

	@Autowired
	private CityRepository repository;
	
	@Autowired
	private UFService ufService;	
	
	public City findById(Long id) {
		return this.repository.findById(id).orElseThrow(
				() -> new EntityNotFoundException(String.format(MSG_CITY_NOT_FOUND, id)));
	}
	
	public List<City> list() {
		return this.repository.findAll();
	}	
	
	public void persist(City city) {		
		try {
			
			if(ObjectUtils.isEmpty(city.getName()))
				throw new ValueRequiredException("O campo nome é obrigatório");
			
			if(repository.existsByName(city.getName()))
				throw new EntityRegistredException(String.format("Cidade de nome %s já cadastrada", city.getName()));
			
			if(ObjectUtils.isEmpty(city.getUf()) || ObjectUtils.isEmpty(city.getUf().getId()))
				throw new ValueRequiredException("UF obrigatório");
			
			ufService.findById(city.getUf().getId());
			
			this.repository.save(city);
			
		}catch(EntityNotFoundException e) {
			throw new EntityDependenceNotFoundException(String.format("UF de código %d não encontrado",city.getUf().getId()));			
		}	
		
	}	
	
	public void delete(Long id) {		
		try {		
			
			this.repository.deleteById(id);
			
		}catch(EmptyResultDataAccessException e) {
			throw new EntityNotFoundException(String.format("Cidade de código %d não encontrada",id));
			
		}catch (DataIntegrityViolationException ex) {
			throw new EntityInUseException(String.format("Cidade de código %d está em uso",id));
		}
	}
}
