package com.algaworks.meat.domain.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.algaworks.meat.domain.exception.EntityInUseException;
import com.algaworks.meat.domain.exception.EntityNotFoundException;
import com.algaworks.meat.domain.exception.ValueRequiredException;
import com.algaworks.meat.domain.model.Product;
import com.algaworks.meat.domain.repository.ProductRepository;

@Service
public class ProductService {
	
	private static final String MSG_PRODUCT_NOT_FOUND = "Produto de código %d não encontrado";
	
	@Autowired
	private ProductRepository repository;
	
	public Product findById(Long id) {
		return this.repository.findById(id).orElseThrow(
				() -> new EntityNotFoundException(String.format(MSG_PRODUCT_NOT_FOUND,id)));
	}
	
	public List<Product> list() {
		return this.repository.findAll();
	}	
	
	public void persist(Product product) {		
		if(ObjectUtils.isEmpty(product.getName()))
			throw new ValueRequiredException("O campo nome é obrigatório");		
		
		this.repository.save(product);		
	}	
	
	public void delete(Long id) {
		try {		
			
			this.repository.deleteById(id);
			
		}catch(EmptyResultDataAccessException e) {
			throw new EntityNotFoundException(String.format(MSG_PRODUCT_NOT_FOUND,id));
			
		}catch (DataIntegrityViolationException ex) {
			throw new EntityInUseException(String.format("Produto de código %d está em uso",id));
		}
	}

}
