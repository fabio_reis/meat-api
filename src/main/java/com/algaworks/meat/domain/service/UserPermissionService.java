package com.algaworks.meat.domain.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import com.algaworks.meat.domain.exception.EntityNotFoundException;
import com.algaworks.meat.domain.exception.ValueRequiredException;
import com.algaworks.meat.domain.model.Group;
import com.algaworks.meat.domain.model.PaymentMethod;
import com.algaworks.meat.domain.model.Restaurant;
import com.algaworks.meat.domain.model.UserPermission;
import com.algaworks.meat.domain.repository.UserPermissionRepository;

@Service
public class UserPermissionService {
	
	@Autowired
	private UserPermissionRepository repository;
	
	@Autowired
	private GroupService groupService;
	
	public List<UserPermission> listUserPermissions(Long groupId) {
		Group group = groupService.findById(groupId);
		return group.getPermissions();
	}
	
	public UserPermission findById(Long groupId, Long userPermissionId) {
		Group group = groupService.findById(groupId);
		return group.getPermissions()
				.stream()
				.filter(userPermission -> userPermission.getId() == userPermissionId)
				.findFirst()
				.orElseThrow(() -> new EntityNotFoundException("permissão não encontrada"));
				
	}
	
	public void persist(Long groupId, UserPermission userPermission) {
		
		if(ObjectUtils.isEmpty(userPermission.getName()))
			throw new ValueRequiredException("Campo nome é obrigatorio");
		
		Group group = groupService.findById(groupId);
		
		group.getPermissions()
			.add(userPermission);
		
		groupService.persist(group);		
		
	}

	@Transactional
	public void dissassociateUserPermission(Long groupId, Long userPermissionId) {
		Group group = groupService.findById(groupId);
		UserPermission userPermission = findById(groupId, userPermissionId);
		group.getPermissions().remove(userPermission);
	}
	
	@Transactional
	public void associateUserPermission(Long groupId, Long userPermissionId) {
		Group group = groupService.findById(groupId);
		UserPermission userPermission = findById(groupId, userPermissionId);
		group.getPermissions().add(userPermission);
	}

}
