package com.algaworks.meat.domain.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.algaworks.meat.domain.exception.EntityInUseException;
import com.algaworks.meat.domain.exception.EntityNotFoundException;
import com.algaworks.meat.domain.exception.EntityRegistredException;
import com.algaworks.meat.domain.exception.ValueRequiredException;
import com.algaworks.meat.domain.model.PaymentMethod;
import com.algaworks.meat.domain.repository.PaymentMethodRepository;

@Service
public class PaymentMethodService {
	
	private static final String MSG_PAYMENT_METHOD_NOT_FOUND = "Método de pagamento de código %d não encontrado";
	
	@Autowired
	private PaymentMethodRepository repository;
	
	public PaymentMethod findById(Long id) {
		return this.repository.findById(id).orElseThrow(
				() -> new EntityNotFoundException(String.format(MSG_PAYMENT_METHOD_NOT_FOUND, id)));
	}
	
	public List<PaymentMethod> list() {
		return this.repository.findAll();
	}	
	
	public void persist(PaymentMethod paymentMethod) {	
		
		if(ObjectUtils.isEmpty(paymentMethod.getDescription()))
			throw new ValueRequiredException("O campo descrição é obrigatório");
		
		if(repository.existsByDescription(paymentMethod.getDescription()))
			throw new EntityRegistredException(String.format("Método de pagamento de descrição %s já cadastrado",
					paymentMethod.getDescription()));
		
		this.repository.save(paymentMethod);		
		
	}	
	
	public void delete(Long id) {		
		try {		
			
			this.repository.deleteById(id);
			
		}catch(EmptyResultDataAccessException e) {
			throw new EntityNotFoundException(String.format(MSG_PAYMENT_METHOD_NOT_FOUND,id));			
		}catch (DataIntegrityViolationException ex) {
			throw new EntityInUseException(String.format("Método de pagamento de código %d está em uso",id));
		}
	}	

}
