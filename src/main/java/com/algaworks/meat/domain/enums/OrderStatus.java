package com.algaworks.meat.domain.enums;

public enum OrderStatus {
	
	CREATED(1,"Created"),
	CONFIRMED(2,"Confirmed"),
	DELIVERED(3,"Delivered"),
	CANCELLED(4,"Cancelled");
	
	private Integer code;	
	private String description;
	
	OrderStatus(Integer code, String description) {
		this.code = code;	
		this.description = description;
	}

	public static OrderStatus parse(final int code) {		
		for(OrderStatus enumerator : OrderStatus.values()) {
			if(enumerator.code == code)
				return enumerator;
		}
		return null;
	}
	
	public static OrderStatus parse(final String description) {		
		for(OrderStatus enumerator : OrderStatus.values()) {
			if(enumerator.description.equalsIgnoreCase(description))
				return enumerator;
		}
		return null;
	}

}
