package com.algaworks.meat.domain.service;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.TestPropertySource;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import util.ResourceUtils;

@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestPropertySource("/application-test.properties")
public class KitchenServiceIT {	
	
	@LocalServerPort
	private int port;
	
	private static final String KITCHEN_WITHOUT_NAME =
			ResourceUtils.getContentFromResource("/json/kitchen/kitchenWithoutName.json");
	
	private static final String KITCHEN_POST =
			ResourceUtils.getContentFromResource("/json/kitchen/KitchenInputPost.json");
	
	private static final String KITCHEN_PUT =
			ResourceUtils.getContentFromResource("/json/kitchen/KitchenInputPut.json");	
	
	
	@BeforeEach
	public void setup() {		
		RestAssured.requestSpecification = new RequestSpecBuilder()		
				.setContentType(ContentType.JSON)
				.build()	
					.basePath("/kitchens")
					.port(port)			     	
			     	.header("Accept", "application/json");
		RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();		
	}	
	
	@Test()	
	public void shouldFailKitchenWithoutName() {				
		RestAssured.given()					
        	.body(KITCHEN_WITHOUT_NAME)        	
	    .when()
	        .post()
	    .then()
	        .statusCode(HttpStatus.BAD_REQUEST.value());							
	}
	
	@Test
	@Order(1)
	public void shouldPersist() {			
		RestAssured.given()		
        	.body(KITCHEN_POST)        	
	    .when()
	        .post()	        
	    .then()	    	
	        .statusCode(HttpStatus.CREATED.value());							
	}
	
	@Test
	@Order(2)
	public void shouldFailKitchenSameName() {						
		RestAssured.given()					
        	.body(KITCHEN_POST)        	
	    .when()
	        .post()
	    .then()
	        .statusCode(HttpStatus.CONFLICT.value());							
	}
	
	@Test
	@Order(3)
	public void shouldfindKitchen() {			
		RestAssured.given()			
		.when()
			.get("/1")			
		.then()						
			.statusCode(HttpStatus.OK.value());		
	}
	
	@Test
	@Order(4)
	public void shouldPutKitchen() {			
		RestAssured.given()						
        	.body(KITCHEN_PUT)        	
	    .when()
	        .put("/1")	        
	    .then()
	        .statusCode(HttpStatus.OK.value());				
	}	
	
	@Test
	@Order(5)
	public void shouldFailDeleteKitchenInUser() {			
		RestAssured.given()					
			.when()
				.delete("/1")
			.then()			
				.statusCode(HttpStatus.CONFLICT.value());		
	}
	
	@Test
	@Order(6)
	public void shouldFailFindKitchenNonexistent() {			
		RestAssured.given()					
			.when()
				.get("/0")
			.then()		
				.body("detail", Matchers.is("Cozinha de código 0 não encontrada"))
				.statusCode(HttpStatus.NOT_FOUND.value());
	}

}
