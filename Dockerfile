FROM openjdk:11-jre-slim

WORKDIR  /deploy

ARG JAR_FILE

COPY target/${JAR_FILE} /deploy/meat-api.jar

EXPOSE 80

CMD ["java", "-jar", "meat-api.jar"]